import { parseData, shouldFailDataParsingErros, isType, SimpleSchema } from "../../../src/share/utils/SchemaUtil";

const loginSchema = {
    type: Object,
    schema: {
        username: {
            type: String
        },
        password: {
            type: String
        }
    }
};

const objectSchema = {
    type: Object,
    schema: {
        action: { type: String },
        params: {
            type: Object,
            schema: {
                name: { type: String },
                caseSensetive: { type: Boolean }
            }
        }
    }
};

const arraySchema = {
    type: Array,
    schema: {
        type: Object,
        schema: {
            action: { type: String },
            data: {
                type: Array,
                schema: {
                    type: String
                }
            }
        }
    }
};

describe('SchemaUtil', () => {

    describe('parseData', () => {
        test('Null value given to parseData ', () => {
            expect(() => parseData()).toThrow();
        });

        test('Missing Mandatory Data', () => {
            const { data, errors } = parseData(loginSchema, {});
            expect(errors).toHaveLength(2);
            expect(errors).toContainEqual({
                error: 'Missing Mandatory Data',
                param: 'username',
                type: 'String',
                msg: `Missing mandatory data { username: String }`
            });
            expect(errors).toContainEqual({
                error: 'Missing Mandatory Data',
                param: 'password',
                type: 'String',
                msg: `Missing mandatory data { password: String }`
            });
            expect(data).toEqual({});

            expect(shouldFailDataParsingErros(errors)).toBeTruthy();
            expect(shouldFailDataParsingErros(errors, { noMissingMandatory: true })).toBeTruthy();
            expect(shouldFailDataParsingErros(errors, { noMissingMandatory: false })).toBeFalsy();
        });

        test('Incorrect Type', () => {
            const { data, errors } = parseData(loginSchema, {
                username: 'username',
                password: 1234567890
            });
            expect(errors).toHaveLength(1);
            expect(errors).toContainEqual({
                error: 'Wrong Type',
                param: 'password',
                value: 1234567890,
                type: 'String',
                msg: `Incorrect Type: expected String for password`
            });
            expect(data).toEqual({
                username: 'username',
                password: 1234567890
            });

            expect(shouldFailDataParsingErros(errors)).toBeTruthy();
            expect(shouldFailDataParsingErros(errors, { enforceTyping: true })).toBeTruthy();
            expect(shouldFailDataParsingErros(errors, { enforceTyping: false })).toBeFalsy();
        });

        test('Reduntant Data', () => {
            const { data, errors } = parseData(loginSchema, {
                username: 'username',
                password: 'password',
                email: 'test@test.test'
            });
            expect(errors).toHaveLength(1);
            expect(errors).toContainEqual({
                error: 'Reduntant',
                param: 'email',
                value: 'test@test.test',
                msg: `Reduntant data: { email }`
            });
            expect(data).toEqual({
                username: 'username',
                password: 'password'
            });

            expect(shouldFailDataParsingErros(errors)).toBeFalsy();
            expect(shouldFailDataParsingErros(errors, { noReduntant: true })).toBeTruthy();
            expect(shouldFailDataParsingErros(errors, { noReduntant: false })).toBeFalsy();
        });

        test('Successfully Parsed Data', () => {
            const { data, errors } = parseData(loginSchema, {
                username: 'username',
                password: 'password'
            });
            expect(errors).toBeNull();
            expect(data).toEqual({
                username: 'username',
                password: 'password'
            });
        });

        test('No error at missing optional data', () => {
            const { data, errors } = parseData({ ...loginSchema, email: { type: String, optional: true } }, {
                username: 'username',
                password: 'password'
            });
            expect(errors).toBeNull();
            expect(data).toEqual({
                username: 'username',
                password: 'password'
            });
        });

        test('Valid Object Check', () => {
            const { data, errors } = parseData(objectSchema, {
                action: 'search',
                params: {
                    name: 'E-Channel',
                    caseSensetive: false
                }
            });
            expect(errors).toBeNull();
            expect(data).toEqual({
                action: 'search',
                params: {
                    name: 'E-Channel',
                    caseSensetive: false
                }
            });
        });

        test('Invalid Object Check', () => {
            const { data, errors } = parseData(objectSchema, {
                action: 'search',
                params: {
                    name: 'E-Channel',
                    caseSensetive: 'false',
                    extra: 'useless'
                }
            });
            expect(errors).toHaveLength(2);
            expect(errors).toContainEqual({
                error: 'Wrong Type',
                param: 'caseSensetive',
                value: 'false',
                type: "Boolean",
                msg: `Incorrect Type: expected Boolean for caseSensetive`
            });
            expect(errors).toContainEqual({
                error: 'Reduntant',
                param: 'extra',
                value: 'useless',
                msg: `Reduntant data: { extra }`
            });
            expect(data).toEqual({
                action: 'search',
                params: {
                    name: 'E-Channel',
                    caseSensetive: 'false'
                }
            });
        });

        test('Valid Array Check', () => {
            const { data, errors } = parseData(arraySchema, [{
                action: 'search',
                data: ['E-Channel']
            }]);
            expect(errors).toBeNull();
            expect(data).toEqual([{
                action: 'search',
                data: ['E-Channel']
            }]);
        });

        // test('Primitive Data Check', () => {
        //     const { data, errors } = parseData({ type: String }, 'string');
        //     expect(errors).toBeNull();
        //     expect(data).toEqual('string');
        // });

    });


    describe('isType', () => {

        test('Null value given to isType ', () => {
            expect(() => isType(null, 'string')).toThrow();
            expect(() => isType()).toThrow();
            expect(() => isType(undefined, 'teststring')).toThrow();
            expect(isType(String, undefined)).toBeFalsy();
        });

        test('Unsupported type check in isType ', () => {
            expect(() => isType(3, '', 'SOME_WEIRD_TYPE_THAT_DOES_NOT_EXIST')).toThrow();
        });

        test('Primitive type check', () => {
            expect(isType(String, '123')).toBeTruthy();
            expect(isType(String, 123)).toBeFalsy();

            expect(isType(Number, 123)).toBeTruthy();
            expect(isType(Number, '123')).toBeFalsy();

            expect(isType(Boolean, true)).toBeTruthy();
            expect(isType(Boolean, false)).toBeTruthy();
            expect(isType(Boolean, undefined)).toBeFalsy();
            expect(isType(Boolean, null)).toBeFalsy();
        });

        test('Object Check', () => {
            expect(isType(Object, {
                action: 'search',
                params: {
                    name: 'E-Channel',
                    caseSensetive: false
                }
            })).toBeTruthy();
        });
    });

    describe('Simple Schema', () => {
        test('Object Conversion', () => {
            const result = SimpleSchema({
                username: String,
                password: String
            });
            expect(result).toEqual(loginSchema);
        });

        test('Array Conversion', () => {
            const result = SimpleSchema([
                {
                    action: String,
                    data: [String]
                }
            ]);
            expect(result).toEqual(arraySchema);
        });

        test('With options', () => {
            const result = SimpleSchema({
                level: [String, { optional: true }]
            });
            expect(result).toEqual({
                type: Object,
                schema: {
                    level: {
                        type: String,
                        optional: true
                    }
                }
            });
        });
    });
});
