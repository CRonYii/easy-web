# Easy-Web

-----------------

Easy-Web is a fullstack javascript solution intergrated into one single project.
It uses Node Express as the backend server, mongoose to connect to MongoDB for providing RESTful APIs services.
It uses React for building the front-end Web Application.

Everything is intergrated for you to out-of-box uses.

# Install

```sh
$ git clone https://CRonYii@bitbucket.org/CRonYii/easy-web.git
$ cd easy-web
$ npm install
```

# Set up the Project
### Environment Variables
Create a `.env.dev` file in the root directory, here is an example of the required environment variables:

```
PORT=8080
MONGODB_URL=mongodb://localhost:27017/e-channel
SESSION_SECRET=secret
MYSQL_HOST=localhost
MYSQL_USER=root
MYSQL_PASSWORD=password
```

### MongoDB
The Starter kit uses and only uses MongoDB to store data.
Make sure you have MongoDB installed and running, and the MONGODB_URL environment variable is updated accordingly.

# Run the project

### development
To run the development environment, run:

```sh
$ npm run dev
```
It will start the server at the specifed PORT, and will watch for source code changes.

### production

To run a production build, run:

```sh
$ npm run build
```
It will run a optimized production build into /build folder.