import { Breadcrumb, Layout, Menu, Button } from 'antd';
import React from 'react';
import { RestfulApis, ServiceApis } from '../share/ApiDefinition/ApiDefinition';
import { getAllApisFromDefinition } from '../share/ApiDefinition/ApiDefinitionGenerator';
import { CollapseList } from './components/CollapseList';
import { createRequestFormByApi } from './components/RequestForm';
import { subscribeApp } from './utils/StateUtil';

const { Header, Content, Footer } = Layout;

const apiList = (() => {
    function toArray(apiDefs) {
        const allApis = getAllApisFromDefinition(apiDefs);
        return Object.keys(allApis).map((name) => {
            const apiDef = allApis[name];
            return { name, apiDef };
        });
    }

    const itemList = toArray(ServiceApis).concat(toArray(RestfulApis)).map(({ name, apiDef }) => {
        return { header: name, component: createRequestFormByApi(apiDef) }
    });

    return <CollapseList itemList={itemList} />
})();

export const App = subscribeApp(
    class extends React.Component {

        render() {
            const app = this.props.container;
            const appState = app.state;
            return <Layout className="layout" style={{ minHeight: '100vh' }}>
                <Header>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['2']}
                        style={{ lineHeight: '64px' }}
                    >
                        <Menu.Item key="1">nav 1</Menu.Item>
                        <Menu.Item key="2">nav 2</Menu.Item>
                        <Menu.Item key="3">nav 3</Menu.Item>
                    </Menu>
                </Header>
                <Content style={{ padding: '0 50px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                    <Button onClick={() => {throw new Error()}}>Throw an Error</Button>
                    <div style={{ background: '#fff', padding: 24, height: '100%' }}>
                        API Test
                        {apiList}
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    Ant Design ©2018 Created by Ant UED
        </Footer>
            </Layout >
        }

    })