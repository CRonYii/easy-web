import React from 'react';
import { Form, Input, Button, Col, Row } from 'antd';
import { endpoint } from '../utils/ApiUtil';

// TODO: maybe implement a rescursive input
export const createRequestFormByApi = (apiDef) => {
    const { accept } = apiDef;
    const inputFields = Object.keys(accept.schema);
    const onSubmit = endpoint(apiDef);

    return <RequestForm inputFields={inputFields} onSubmit={onSubmit} />
}
export const RequestForm = Form.create()(class extends React.Component {

    state = { result: '' };

    onSubmit = () => {
        this.props.form.validateFields((errors, values) => {
            if (!errors) {
                Object.keys(values).forEach(key => {
                    const value = values[key];
                    if (value == "") 
                        delete values[key];
                });
                this.props.onSubmit(values)
                    .then(res => {
                        if (res) {
                            this.setState({ result: res.data });
                        }
                    })
                    .catch((err) => {
                        this.setState({ result: err });
                    });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { inputFields } = this.props;

        return (
            <Row align="top">
                <Col xs={24} lg={12}>
                    <Form layout='vertical' style={{ maxWidth: 300 }}>
                        {
                            inputFields.map((name, index) => {
                                return (<Form.Item key={index}>
                                    {getFieldDecorator(name)(<Input placeholder={name} />)}
                                </Form.Item>);
                            })
                        }
                        <Button onClick={this.onSubmit} htmlType='submit' type='primary'>Submit</Button>
                    </Form>
                </Col>
                <Col xs={24} lg={12}>
                    Result
                    <pre style={{ color: 'white', backgroundColor: 'black', width: '100%', maxHeight: '50vh', overflow: 'scroll' }}>
                        {JSON.stringify(this.state.result, null, 2)}
                    </pre>
                </Col>
            </Row>
        );
    }

});