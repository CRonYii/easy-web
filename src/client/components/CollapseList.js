import React from 'react';
import { Collapse, Input } from 'antd';

export class CollapseList extends React.Component {

    state = {
        filter: ''
    }

    isSearched = (name) => {
        return this.state.filter.split('').every(char => name.toLowerCase().includes(char));
    }

    render() {
        return <div>
            <Input placeholder='Filter' onChange={(event) => this.setState({ filter: event.target.value.toLowerCase() })} />
            <Collapse>
                {
                    this.props.itemList.map(({ header, component }) => {
                        return <Collapse.Panel header={header} key={header} style={{ display: this.isSearched(header) ? 'block' : 'none' }}>
                            {component}
                        </Collapse.Panel>
                    })
                }
            </Collapse>
        </div>;
    }

}