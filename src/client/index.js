import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import { Provider } from 'unstated';

// use babel.config.js to generate different babel config for client/server build
ReactDOM.render(
    <Provider>
        <App />
    </Provider>,
    document.querySelector("#root")
);