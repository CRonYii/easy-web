import { parseData, shouldFailDataParsingErros } from "../../share/utils/SchemaUtil";
import Axios from "axios";

const useErrorHandler = (handler, status, error) => {
    if (handler && handler[status] && handler[status] instanceof Function) {
        handler[status](error);
        return true;
    } else {
        return false;
    }
};

export const makeApiEndpointGeneator = (generatorOption = {}) => {
    return (apiDef, apiOption = {}) => {

        const getOption = (key) => {
            return apiOption[key] || (apiOption[key] !== false && generatorOption[key])
        };

        return async (givenData, callOption = {}) => {
            const { data, errors } = parseData(apiDef.accept, givenData, { typeCheck: getOption('typeCheck') });

            if (errors) {
                if (generatorOption.handleDataError) {
                    generatorOption.handleDataError(errors)
                } else {
                    console.warn(`API Data parsing error: `, errors);
                }

                if (shouldFailDataParsingErros(errors, {
                    noReduntant: getOption('noReduntant'),
                    enforceTyping: getOption('enforceTyping'),
                    noMissingMandatory: getOption('noMissingMandatory')
                })) {
                    return Promise.reject({ status: 'Failed parsing data.', errors });
                }
            }

            const { method } = apiDef;
            const url = apiDef.keyed ? apiDef.getFullPath() + '/' + givenData.id : apiDef.getFullPath();

            const axiosConfig = {
                method,
                url
            };

            if (method === 'get') {
                axiosConfig['params'] = data;
            } else {
                axiosConfig['data'] = data;
            }

            return Axios(axiosConfig).catch(err => {
                if (err.response) {
                    return err.response;
                } else {
                    console.error(`Unexpected API Call error: `, err);
                }
            }).then((res) => {
                const { status } = res;
                if (status >= 400) {
                    let errorHandled = useErrorHandler(callOption.onError, status, res.data);
                    if (!errorHandled) {
                        errorHandled = useErrorHandler(apiOption.onError, status, res.data);
                    }
                    if (!errorHandled) {
                        errorHandled = useErrorHandler(generatorOption.defaultErrorHandler, status, res.data);
                    }
                    if (!errorHandled) {
                        throw new Error(`Uncaught Error at Status: ${status}`);
                    }
                }

                return res;
            }).then((res) => {
                const { errors } = parseData(apiDef.response, res.data);

                if (errors) {
                    if (shouldFailDataParsingErros(errors, {
                        noMissingMandatory: getOption('noMissingMandatory')
                    })) {
                        return Promise.reject({ status: 'Invalid Response Data', errors });
                    }
                }

                return res;
            });
        }
    };
};