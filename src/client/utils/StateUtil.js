import React from 'react';
import { Provider, Subscribe, Container } from 'unstated';

export class AppContainer extends Container {

    state = {
        count: 0
    }

    increment() {
        this.setState({ count: this.state.count + 1 });
    }

    decrement() {
        this.setState({ count: this.state.count - 1 });
    }

}

export const makeSubscriber = (Container) => {
    return (Component) => {
        return class extends React.Component {
            render() {
                return (<Subscribe to={[Container]}>
                    {
                        (container) => {
                            return <Component {...this.props} container={container} />;
                        }
                    }
                </Subscribe>);
            };
        }
    }
}

export const subscribeApp = makeSubscriber(AppContainer);