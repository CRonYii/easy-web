import { notification } from "antd";
import { RestfulApis, ServiceApis } from "../../share/ApiDefinition/ApiDefinition";
import { makeApiEndpointGeneator } from "./ApiEndpointGenerator";

export const endpoint = makeApiEndpointGeneator({
    noMissingMandatory: true,
    defaultErrorHandler: {
        401: () => {
            notification.error({ message: 'Unauthorized', description: 'You are not authorized to perform the action.' });
        },
        422: ({ error, errors }) => {
            if (error)
                notification.error({ message: 'Invalid Input Data', description: error });
            if (errors)
                errors.map((err) => `${err.param}: ${err.msg}`)
                    .forEach((description) =>
                        notification.error({ message: 'Invalid Input Data', description })
                    );
        }
    }
});

export const callEndpoint = (apiDef) => endpoint(apiDef)();
export class ApiUtil {

    static login = endpoint(ServiceApis.v1.userServices.login);

    static logout = endpoint(ServiceApis.v1.userServices.logout);

    static register = endpoint(RestfulApis.users.create);

    static getUserInfo = endpoint(RestfulApis.users.index);

}