import mongoose from 'mongoose';

const appInfoSchema = {
    mysqlVersion: Number,
    executedSQLScripts: Array
};

const appInfoMongoSchema = new mongoose.Schema(appInfoSchema);

appInfoMongoSchema.statics.getMeta = async function () {
    const { mysqlVersion, executedSQLScripts } = await this.findOne().exec();
    return { mysqlVersion, executedSQLScripts };
};

appInfoMongoSchema.statics.updateMeta = async function ({ mysqlVersion }) {
    const meta = await this.findOne().exec();
    meta.mysqlVersion = mysqlVersion;
    meta.executedSQLScripts.push(mysqlVersion);
    return meta.save();
};

appInfoMongoSchema.statics.initMeta = async function () {
    return AppInfo.find().exec().then((result) => {
        if (result.length === 0) {
            return new AppInfo({ mysqlVersion: 0, executedSQLScripts: [] }).save();
        }
    });
};

const AppInfo = mongoose.model('AppInfo', appInfoMongoSchema);

export { AppInfo, appInfoSchema };

