import mongoose from 'mongoose';

const logSchema = {
    timestamp: { type: Date, default: Date.now, index: true },
    level: { type: String, index: true },
    message: String
};

const logMongoSchema = new mongoose.Schema(logSchema);

const Log = mongoose.model('Log', logMongoSchema);

export { Log, logSchema };

