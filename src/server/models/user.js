import mongoose from 'mongoose';
import logger from '../utils/logger';
import bcrypt from "bcrypt-nodejs";

const userSchema = {
    username: { type: String, unique: true },
    email: { type: String, unique: true },
    role: Map,
    password: String,
    lastLogin: Date
};

const userMongoSchema = new mongoose.Schema(userSchema);

userMongoSchema.statics.hashPassword = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) reject(err);
            bcrypt.hash(password, salt, null, function (err, hash) {
                if (err) reject(err);
                else resolve(hash);
            });
        });
    });
};

userMongoSchema.methods.register = async function () {
    const { username, email } = this;
    const existingUser = await this.model('User').findOne().or([{ username }, { email }]).exec().catch(err => logger.error(`When registering user: ${err}`));
    if (existingUser) {
        return Promise.reject({ duplicate: true });
    } else {
        return this.save();
    }
};

userMongoSchema.methods.comparePassword = function (password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password, function (err, res) {
            if (err) reject(err);
            else resolve(res);
        });
    }).catch(err => {
        logger.error(`When comparing password for ${this.username}: ${err}`);
    });
};

userMongoSchema.pre('save', async function (next) {
    if (!this.isModified("password")) {
        return next();
    }
    this.password = await this.model('User').hashPassword(this.password);
    next();
});

const User = mongoose.model('User', userMongoSchema);

export { User, userSchema };
