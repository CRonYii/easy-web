import logger from './logger';
import { Router } from 'express';
import { validateBody } from './validationUtil';
import { convertToJSON } from '../../share/utils/ConversionUtil';

const filterObject = (raw, allowed) => {
    const keyMap = { _id: 'id' };
    return Object.keys(raw)
        .filter(key => {
            if (allowed instanceof Array) return allowed.includes(key);
            else return allowed[key];
        })
        .reduce((obj, key) => {
            const returnKey = keyMap[key] || key;
            const returnValue = convertToJSON(raw[key]);
            obj[returnKey] = returnValue;
            return obj;
        }, {});
};

const supportMethods = ['index', 'create', 'read', 'update', 'delete'];
const keyed = ['get', 'read', 'put', 'patch', 'update', 'del', 'delete'];
const map = { index: 'get', list: 'get', read: 'get', create: 'post', update: 'put', modify: 'patch' };

export const ResourceRouter = (route, router) => {
    router = router || Router({ mergeParams: route.mergeParams });

    if (route.middleware) router.use(route.middleware);

    if (route.load) {
        router.param(route.id, (req, res, next, id) => {
            route.load(req, id, (err, data) => {
                if (err) return res.status(404).send(err);
                req[route.id] = data;
                next();
            });
        });
    }

    for (let key in route) {
        const fn = map[key] || key;
        if (router[fn] instanceof Function) {
            const url = ~keyed.indexOf(key) ? ('/:' + route.id) : '/';
            router[fn](url, route[key]);
        }
    }

    return router;
};

export const serveRestfulModel = (model, option) => {
    option = Object.assign({
        router: null,
        defaultSessionValidation: (req) => req.user.isLogin(),
        index: {},
        create: {},
        read: {},
        update: {},
        delete: {}
    }, option);

    const { modelName } = model;

    const getReturnEntity = (entity, req) => {
        let returnSchema = option.returnSchema || { _id: String, ...model.schema.obj };
        if (req && option.getConditionalReturnSchema) {
            returnSchema = option.getConditionalReturnSchema(req);
        }
        return filterObject(entity.toObject(), returnSchema);
    };

    const validateSession = (useSession) => {
        return async (req, res, next) => {
            if (!useSession) {
                next();
            } else {
                if ((useSession instanceof Function ? await useSession(req) : await option.defaultSessionValidation(req))) {
                    next();
                } else {
                    res.sendStatus(401);
                }
            }
        }
    };

    const route = {

        /** Property name to store preloaded entity on `request`. */
        id: modelName,

        middleware: [
            validateSession(option.useSession),
            ...(option.middlewares || []),
        ],

        /** For requests with an `id`, you can auto-load the entity.
         *  Errors terminate the request, success sets `req[id] = data`.
         */
        load: async (req, id, callback) => {
            let entity = await model.findById(id).exec().catch(e => {
                logger.error(e);
            });
            let err = entity ? null : { message: `Cannot find ${modelName}` };
            callback(err, entity);
        }
    };

    const mehtodHandler = {
        /** GET / - List all entities */
        index: async (req, res) => {
            const { query } = req;
            const searchParam = filterObject(query, option.index.searchSchema || option.index.enableAllSearch ? model.schema.obj : {});
            Object.keys(searchParam).forEach((key) => {
                let value = searchParam[key];
                if (!value) {
                    delete searchParam[key];
                    return;
                }
                try {
                    const comparator = value.substring(0, 2);
                    value = value.substring(2)
                    switch (comparator) {
                        case '==':
                            searchParam[key] = { $eq: value };
                            break;
                        case '!=':
                            searchParam[key] = { $ne: value };
                            break;
                        case '@@':
                            searchParam[key] = { $in: JSON.parse(value) };
                            break;
                        case '!@':
                            searchParam[key] = { $nin: JSON.parse(value) };
                            break;
                        case '<=':
                            searchParam[key] = { $lte: value };
                            break;
                        case '>=':
                            searchParam[key] = { $gte: value };
                            break;
                        case '<<':
                            searchParam[key] = { $lt: value };
                            break;
                        case '>>':
                            searchParam[key] = { $gt: value };
                            break;
                        default:
                            break;
                    }
                } catch(e) {
                    logger.error(`When generating search parameter for ${key}:\n` + e);
                }
            });

            const entities = await model.find(searchParam).exec();
            res.json(entities
                .map(entity => getReturnEntity(entity, req))
            );
        },

        /** POST / - Create a new entity */
        create: (req, res) => {
            new model(req.body).save()
                .then((entity) => res.status(201).json(getReturnEntity(entity, req))
                    , (err) => {
                        res.sendStatus(500);
                        logger.error(`When creating ${modelName}: ${err}`);
                    })
                .catch(err => logger.error(`When creating ${modelName}: ${err}`));
        },

        /** GET /:id - Return a given entity */
        read: (req, res) => {
            const entity = req[modelName];
            res.json(getReturnEntity(entity, req));
        },

        /** PUT /:id - Update a given entity */
        update: (req, res) => {
            const entity = req[modelName];
            entity.updateOne(req.body)
                .then(() => res.sendStatus(204),
                    err => {
                        res.sendStatus(500);
                        logger.error(`When updating ${modelName}: ${err}`);
                    })
                .catch(err => logger.error(`When updating ${modelName}: ${err}`));
        },

        /** DELETE /:id - Delete a given entity */
        delete: (req, res) => {
            const entity = req[modelName];
            entity.remove()
                .then(() => res.sendStatus(204),
                    err => {
                        res.sendStatus(500);
                        logger.error(`When deleting ${modelName}: ${err}`);
                    })
                .catch(err => logger.error(`When deleting ${modelName}: ${err}`));
        }

    };

    supportMethods.forEach(method => {
        if (option[method].enabled === true || (option[method].enabled !== false && option.allEnabled)) {
            route[method] = [
                validateSession(option[method].useSession),
                ...validateBody(option[method].validation),
                ...(option[method].middlewares || []),
                option[method].handler || mehtodHandler[method]
            ];

        }
    });

    return ResourceRouter(route, option.router);
}