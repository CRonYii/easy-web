import { validationResult, check } from 'express-validator/check';

export const validateBody = (validation) => {
    if (validation instanceof Array) {
        validation.push((req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            } else {
                next();
            }
        });
        return validation;
    }
    return [];
};

const getCheckByType = (type) => {
    
}

export const getValidationMiddlewares = (schema) => {
    const result = [];
    
    Object.keys(schema).forEach(key => {
        const type = schema[key];
        check(key).isString()
    });

    return result;
}