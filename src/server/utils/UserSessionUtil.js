import { User } from "../models/user";

// TODO: Consider move all the user/session implementation to mysql
export class UserUtil {

    constructor(session) {
        this.session = session;
    }

    login = async (userLogin, password) => {
        const user = await User.findOne().or([{ username: userLogin }, { email: userLogin }]).exec();
        if (user) {
            return this.loginById(user.id, password);
        } else {
            return Promise.reject({ invalidCrendentials: true });
        }
    }

    loginById = async (id, password) => {
        const user = await User.findById(id).exec();
        const passwordMatch = user.comparePassword(password);
        if (passwordMatch) {
            // update last login date
            user.lastLogin = new Date();
            user.save();

            this.session.user = id;
            return Promise.resolve();
        } else {
            return Promise.reject({ invalidCrendentials: true });
        }
    }

    logout = () => {
        this.session.destroy();
    }

    isLogin = () => {
        return this.session.user != undefined;
    }

    getUser = () => {
        return User.findById(this.session.user).exec();
    }

    isRole = async (role) => {
        if (!this.isLogin()) return false;
        const user = await this.getUser();
        if (!user.role) return false;
        return user.role.get(role);
    }

}

export const UserSessionMiddleware = (req, res, next) => {
    req.user = new UserUtil(req.session);
    next();
};  