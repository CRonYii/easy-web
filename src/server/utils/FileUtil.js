import fs from 'fs';
import path from 'path';

const readFile = (filePath, option) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, filePath), option, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

export { readFile };