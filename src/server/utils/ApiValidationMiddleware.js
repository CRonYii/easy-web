import logger from "./logger";
import { parseData, shouldFailDataParsingErros } from "../../share/utils/SchemaUtil";

export const createApiValidationMiddleware = (
    option = {
        logAtInvalidResponse: false
    }) => {

    return (apiDefs) => {
        return (req, res, next) => {

            const apiDef = apiDefs.getApi(req.originalUrl, req.method.toLowerCase());
            if (!apiDef) {
                return next();
            }
            const { accept, response } = apiDef;

            // pre-check
            const data = req.method === 'GET' ? req.query : req.body;
            const { errors } = parseData(accept, data);

            if (errors) {
                if (shouldFailDataParsingErros(errors)) {
                    return res.status(422).json({ errors });
                }
            }

            const originalFunction = res.json.bind(res);

            // post-check
            res.json = (body) => {
                const { errors } = parseData(response, body);
                if (errors && option.logAtInvalidResponse) {
                    logger.error(`${apiDef.getFullPath()}: erros on response ${JSON.stringify(errors)}`);
                }
                originalFunction(body);
            }

            next();
        };
    };
};