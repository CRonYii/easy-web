import mysql from 'mysql';
import { readFile } from './FileUtil';
import logger from './logger';
import { AppInfo } from '../models/appInfo';

const readSQLFile = async (version) => {
    return readFile(`../../../deployment/sql/v${version}.sql`, 'utf8');
};

const getLatestSQLVersion = async () => {
    return readFile(`../../../deployment/sql/metadata.json`, 'utf8')
        .then((data) => JSON.parse(data))
        .then(({ version }) => version);
};

export const connectMySQL = (mysqlOption) => {
    const createPool = (option) => {
        return new MySQLPool(mysql.createPool({ ...mysqlOption, ...option }));
    };

    const createConnection = (option) => {
        return new MySQLConnection(mysql.createConnection({ ...mysqlOption, ...option }));
    };

    const runMultiLineSQL = async (sql) => {
        const connection = createConnection({ multipleStatements: true });

        return connection.beginTransaction()
            .then(() => connection.query(sql))
            .then(() => connection.commit())
            .then(() => connection.end())
    }

    const runSQLScripts = async () => {
        logger.info(`Server Start SQL Scripts run`);

        const latestSQLVersion = await getLatestSQLVersion();
        const { mysqlVersion } = await AppInfo.getMeta();

        async function runSQLVersion(version) {
            if (version > latestSQLVersion) {
                return Promise.resolve();
            }
            logger.info(`running v${version} sql`);
            const sql = await readSQLFile(version);
            await runMultiLineSQL(sql);
            AppInfo.updateMeta({ mysqlVersion: version });
            return runSQLVersion(version + 1);
        }

        const result = await runSQLVersion(mysqlVersion + 1)
            .then(() => {
                logger.info(`Finished Server Start SQL Scripts Run, current server MySQL version is v${mysqlVersion}`);
                return true;
            })
            .catch(err => {
                logger.error(`When running Multi-line SQL: ${err.stack}`);
                return false;
            });

        return result;
    };

    return { createPool, createConnection, runMultiLineSQL, runSQLScripts };
};

class MySQLPool {

    constructor(pool) {
        this.pool = pool;
    }

    query(sql, values) {
        return new Promise((resolve, reject) => {
            this.pool.query(sql, values, function (err, results, fields) {
                if (err) {
                    reject(err);
                } else {
                    resolve({ results, fields });
                }
            });
        });
    }

    getConnection() {
        return new Promise((resolve, reject) => {
            this.pool.getConnection(function (err, connection) {
                if (err) {
                    reject(err);
                } else {
                    resolve(new MySQLPoolConnection(connection));
                }
            });
        });
    }

    end() {
        return new Promise((resolve, reject) => {
            this.pool.end(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

}

class MySQLConnection {

    inTransaction = false;

    constructor(connection) {
        this.connection = connection;
    }

    query(sql, values) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, values, function (err, results, fields) {
                if (err) {
                    if (this.inTransaction)
                        this.rollback();
                    reject(err);
                } else {
                    resolve({ results, fields });
                }
            });
        });
    }

    beginTransaction(option) {
        return new Promise((resolve, reject) => {
            this.connection.beginTransaction(option, function (err) {
                if (err) {
                    reject(err);
                } else {
                    this.inTransaction = true;
                    resolve();
                }
            })
        });
    }

    rollback(option) {
        return new Promise((resolve, reject) => {
            this.connection.rollback(option, function (err) {
                this.inTransaction = false;
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        });
    }

    commit(option) {
        return new Promise((resolve, reject) => {
            this.connection.commit(option, function (err) {
                this.inTransaction = false;
                if (err) {
                    this.rollback();
                    reject(err);
                } else {
                    resolve();
                }
            })
        });
    }

    end() {
        return new Promise((resolve, reject) => {
            this.connection.end(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    destroy() {
        this.connection.destroy();
    }

}

class MySQLPoolConnection extends MySQLConnection {

    release() {
        this.connection.release();
    }

}