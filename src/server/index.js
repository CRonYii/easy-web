import './env';
import bodyParser from 'body-parser';
import compression from 'compression';
import connectMongo from 'connect-mongo';
import express from 'express';
import session from 'express-session';
import http from 'http';
import mongoose from 'mongoose';
import morgan from 'morgan';
import { RestfulApis, ServiceApis } from '../share/ApiDefinition/ApiDefinition';
import api from './api';
import { AppInfo } from './models/appInfo';
import { createApiValidationMiddleware } from './utils/ApiValidationMiddleware';
import logger from "./utils/logger";
import { connectMySQL } from './utils/MySQLUtil';
import { UserSessionMiddleware } from './utils/UserSessionUtil';
import winston from 'winston';
import expressWinston from 'express-winston';

const app = express();
app.server = http.createServer(app);

// TODO: add mysql to docker
async function setupMySQL() {
	// connect to mysql
	const mysqlUtil = connectMySQL({
		connectionLimit: 10,
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_USER,
		password: process.env.MYSQL_PASSWORD,
		database: 'echannel'
	});

	// run sql scripts depends on the current version
	const runScripts = await mysqlUtil.runSQLScripts();

	return runScripts;
}

async function setupMongo() {
	return new Promise(resolve => {
		// connect to MongoDB
		const mongodbURL = process.env.MONGODB_URL;

		mongoose.connect(mongodbURL, { useNewUrlParser: true });

		mongoose.connection.on('error', (e) => {
			logger.error('MongoDB connection error. ' + e);
			resolve(false);
		});

		mongoose.connection.once('open', async () => {
			logger.info('Connected to MongoDB at ' + mongodbURL);
			await AppInfo.initMeta();

			resolve(true);
		});
	});
}

async function startServer() {
	// Dev environment only
	if (process.env.NODE_ENV !== 'production') {
		// logger
		app.use(morgan('dev'));
	}
	app.use(expressWinston.errorLogger({
		transports: [
			new winston.transports.Console()
		],
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.json()
		)
	}));
	app.use(compression());
	app.use(bodyParser.json());

	// mongo must start before mysql can start
	const mongoConnected = await setupMongo();
	const mysqlConnected = await setupMySQL();
	const dbConnected = mongoConnected && mysqlConnected;

	if (!dbConnected) {
		// exit here
		logger.error(`Failed to connect to Database. Server start stopped.`);
		return;
	}

	const MongoStore = connectMongo(session);

	app.use(session({
		secret: process.env.SESSION_SECRET,
		store: new MongoStore({ mongooseConnection: mongoose.connection }),
		resave: true,
		saveUninitialized: true
	}));

	app.use(UserSessionMiddleware);

	const apiValidationMiddleware = createApiValidationMiddleware({ logAtInvalidResponse: true });
	app.use(apiValidationMiddleware(ServiceApis));
	app.use(apiValidationMiddleware(RestfulApis));

	// api router
	app.use('/api', api);

	// serve client webapp
	app.use(express.static(__dirname + './../../build/client'));
	// serve public
	app.use(express.static(__dirname + './../../public'));

	app.server.listen(process.env.PORT, () => {
		logger.info(`Started on port ${app.server.address().port}`);
	});
};

startServer();

export default app;