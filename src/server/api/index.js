import { Router } from 'express';
import restful from './restful';
import v1Services from './services/v1';

const api = Router();

api.use('/restful', restful);
api.use('/v1', v1Services);

export default api;