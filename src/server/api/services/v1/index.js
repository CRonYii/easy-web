import { Router } from 'express';
import userServices from './userServices';

const services = Router();

services.get('/appInfo', (req, res) => {
    res.send({
        version: "0.1.0"
    });
});

services.use('/userServices', userServices);

export default services;