import { Router } from 'express';
import { check } from 'express-validator/check';
import logger from '../../../utils/logger';
import { validateBody } from '../../../utils/validationUtil';

const userServices = Router();

userServices.post('/login',
    validateBody([check('username').not().isEmpty(), check('password').isLength({ min: 9 })]),
    (req, res) => {
        const { username, password } = req.body;
        req.user.login(username, password)
            .then(() => {
                res.sendStatus(200);
            }, (err) => {
                if (err.invalidCrendentials) {
                    res.sendStatus(401);
                } else {
                    logger.error(`When logging in for ${req.body.username}: ${err}`);
                    res.sendStatus(500);
                }
            });
    }
);

userServices.post('/logout', (req, res) => {
    try {
        req.user.logout();
        res.sendStatus(200);
    } catch (err) {
        logger.error(`When logging out for ${req.session.user}: ${err}`);
        res.sendStatus(500);
    }
});

userServices.post('/test', (req, res) => {
    res.json({ message: req.body.message });
});

export default userServices;