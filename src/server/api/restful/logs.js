import { Log } from '../../models/log';
import { serveRestfulModel } from '../../utils/restfulUtil';

export default serveRestfulModel(Log, {
    returnSchema: ['timestamp', 'level', 'message'],
    index: {
        enabled: true,
        enableAllSearch: true,
        useSession: async (req) => await req.user.isRole('admin')
    }
});