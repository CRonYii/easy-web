import { check } from 'express-validator/check';
import { User } from '../../models/user';
import logger from '../../utils/logger';
import { serveRestfulModel } from '../../utils/restfulUtil';

export default serveRestfulModel(User, {
    returnSchema: ['username', 'email', 'role', 'lastLogin'],
    index: {
        enabled: true,
        useSession: (req) => req.user.isLogin()
    },
    create: {
        enabled: true,
        validation: [
            check('role').optional().isArray().custom(async (roles, { req }) => {
                const isAdmin = await req.user.isRole('admin');
                roles.some((role) => {
                    if (!['admin'].includes(role)) throw new Error('Invalid user role');
                    if (role === 'admin' && !isAdmin) {
                        throw new Error('Not Authrozied to create an admin user.');
                    }
                });
                return true;
            }).customSanitizer((roles) => {
                const userRoleMap = new Map();
                roles.forEach((role) => {
                    userRoleMap.set(role, true);
                });
                return userRoleMap;
            }),
            check('username').not().isEmpty().isLength({ max: 20 }),
            check('email').isEmail(),
            check('password').isLength({ min: 9 }).withMessage('Password must be longer and equals to 9 characters')
        ],
        handler: (req, res) => {
            const { username, email, role, password } = req.body;
            new User({
                username,
                role,
                email,
                password,
                lastLogin: new Date()
            })
                .register()
                .then((user) => {
                    req.user.login(user.id, password);
                    res.sendStatus(201);
                }, (err) => {
                    if (err.duplicate) {
                        res.status(422).json({
                            error: 'Duplicate user name or email address'
                        });
                    } else {
                        res.sendStatus(500);
                        logger.error(`Failed to register: ${err}`);
                    }
                });
        }
    }
});