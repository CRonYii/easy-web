import { Router } from 'express';
import users from './users';
import logs from './logs';

const rest = Router();

rest.use('/users', users);
rest.use('/logs', logs);

export default rest;