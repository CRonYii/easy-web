const mapToJSON = (map) => {
    const json = {};
    for (let [key, value] of map) {
        json[key] = value;
    }
    return json;
};

export const convertToJSON = (val) => {
    if (val instanceof Map) return mapToJSON(val);
    return val;
};