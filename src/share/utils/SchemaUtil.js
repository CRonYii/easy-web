export const SimpleSchema = (proto, option) => {
    if (proto instanceof Function) {
        // Type: Construcor Function
        return {
            type: proto,
            ...option
        };
    } else if (proto instanceof Array) {
        const schema = proto[0];
        const option = proto[1];
        if (option) {
            return SimpleSchema(schema, option);
        }
        return {
            type: Array,
            schema: SimpleSchema(schema),
            ...option
        };
    } else if (proto instanceof Object) {
        const schema = {};
        Object.keys(proto).forEach((key) => {
            const value = proto[key];
            schema[key] = SimpleSchema(value);
        });
        return {
            type: Object,
            schema,
            ...option
        };
    }
};

export const isType = (type, obj) => {
    if (!type || !(type instanceof Function))
        throw new Error('Type must be a constructor function.');
    if (obj == null)
        return false;
    // 1st Check: primitive data type using the typeof keyword
    const typeNameMap = {
        [String]: 'string',
        [Number]: 'number',
        [Boolean]: 'boolean',
        [Map]: 'object'
    };

    if (typeNameMap[type])
        return typeof obj === typeNameMap[type];

    // 2nd Check: instanceof
    return obj instanceof type;
}

export const shouldFailDataParsingErros = (errors, option = { noReduntant: false, enforceTyping: true, noMissingMandatory: true }) => {
    if (!errors)
        throw new Error('Errors cannot be null or undefined.');
    const checks = [];
    if (option.noReduntant) {
        checks.push('Reduntant');
    }
    if (option.enforceTyping) {
        checks.push('Wrong Type');
    }
    if (option.noMissingMandatory) {
        checks.push('Missing Mandatory Data');
    };
    return checks.some(checkError => errors.some(({ error }) => error === checkError));
};

// TODO: Consider adding an optional conversion for incorrect type
export const parseData = (schema, givenData, option = {}) => {
    if (!schema)
        throw new Error('Schema cannot be null or undefined.');
    if (!givenData)
        throw new Error('Data cannot be null or undefined.');

    const { typeCheck } = option;

    if (schema.type === Array) {
        return parseArrayTypeData(schema.schema, givenData, option);
    } else if (schema.type === Object) {
        return parseObjectTypeData(schema.schema, givenData, option);
    } else if (schema.type instanceof Function) {
        const errors = [];
        if (typeCheck ? !typeCheck(schema.type, givenData) : !isType(schema.type, givenData)) {
            errors.push({
                error: 'Wrong Type',
                value,
                type: schema.type.name,
                msg: `Incorrect Type: expected ${schema.type.name}`
            })
        }
        return { data: givenData, errors: errors.length !== 0 ? errors : null };
    } else {
        throw new Error(`Unexpected value for Schema: ${schema}`);
    }
};

const parseArrayTypeData = (schema, givenData = [], option = {}) => {
    if (!(givenData instanceof Array))
        throw new Error('Data must be an Array.');

    const data = [];
    let errors = [];
    const { type } = schema;
    const { typeCheck } = option;

    givenData.forEach((item) => {
        if (type === Array || type === Object) {
            const parsedResult = parseData(schema, item, option);
            data.push(parsedResult.data);
            if (parsedResult.errors) {
                errors = errors.concat(parsedResult.errors);
            }
        } else {
            data.push(item);
            if (typeCheck ? !typeCheck(type, item) : !isType(type, item)) {
                errors.push({
                    error: 'Wrong Type',
                    location: 'array',
                    value,
                    type: type.name,
                    msg: `Incorrect Type: expected ${type.name} in array`
                });
            }
        }
    });

    return { data, errors: errors.length !== 0 ? errors : null };
};


const parseObjectTypeData = (schema, givenData = {}, option = {}) => {
    if (!(schema instanceof Object))
        throw new Error('Schema must be an Object.');
    if (!(givenData instanceof Object))
        throw new Error('Data must be an Object.');

    const data = {};
    let errors = [];

    const { typeCheck } = option;

    Object.keys(givenData).forEach((param) => {
        const def = schema[param];
        let value = givenData[param];

        if (!def) {
            errors.push({
                error: 'Reduntant',
                param,
                value,
                msg: `Reduntant data: { ${param} }`
            });
        } else if (value != null) {
            const { type } = def;
            if (typeCheck ? !typeCheck(type, value) : !isType(type, value)) {
                errors.push({
                    error: 'Wrong Type',
                    param,
                    value,
                    type: type.name,
                    msg: `Incorrect Type: expected ${type.name} for ${param}`
                });
            } else if (type === Array || type === Object) {
                const result = parseData(def, value, option);
                value = result.data;
                if (result.errors) {
                    errors = errors.concat(result.errors);
                }
            }

            data[param] = value;
        }
    });

    Object.keys(schema).forEach(param => {
        const { type, optional } = schema[param];
        if (!optional && data[param] == null) {
            errors.push({
                error: 'Missing Mandatory Data',
                param,
                type: type.name,
                msg: `Missing mandatory data { ${param}: ${type.name} }`
            });
        }
    });

    return { data, errors: errors.length !== 0 ? errors : null };
};