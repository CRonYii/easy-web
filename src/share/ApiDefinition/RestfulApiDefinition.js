import { SimpleSchema } from "../utils/SchemaUtil";

export default function createRestfulApiDefinitionBuilder({ RestfulEntity }) {

    class RestfulApiDefinitions {
        constructor(details) {
            Object.keys(details).forEach((key) => {
                try {
                    const item = new RestfulApi(key, details[key]);
                    this[key] = item;
                } catch (err) {
                    console.error(`When creating Restful API definition: ${err}`);
                }
            });
        }

        getApi(url, method) {
            const paths = url.split('/').slice(3);
            const restfulResource = this[paths[0]];
            const keyed = paths[1];
            if (restfulResource) {
                switch (method) {
                    case 'get':
                        if (keyed)
                            return restfulResource['read'];
                        else
                            return restfulResource['index'];
                    case 'post': return restfulResource['create'];
                    case 'put': return restfulResource['update'];
                    case 'delete': return restfulResource['delete'];
                }
            }
        }
    }

    class RestfulApi {

        constructor(name, params) {
            if (!Object.keys(RestfulEntity).includes(name)) throw new Error(`Unsupported Restful Resource`);
            Object.keys(params).forEach(endpoint => {
                this[endpoint] = new RestfulEndpoint({ name, endpoint, ...params[endpoint] });
            });
        }

        getEndpoint(endpoint) {
            return {
                method: RestfulApi.map[endpoint],
                url: this.getFullPath(),
                ...this.supportMethods[endpoint]
            };
        }

    }

    class RestfulEndpoint {

        static supportMethods = ['index', 'create', 'read', 'update', 'delete'];
        static keyed = ['read', 'update', 'delete'];
        static methodMap = { index: 'get', create: 'post', read: 'get', update: 'put', delete: 'delete' };

        static validate(key, value) {
            switch (key) {
                case 'name': return value;
                case 'accept': return SimpleSchema(value);
                case 'response': return SimpleSchema(value);
            }
        }

        constructor(params) {
            if (!RestfulEndpoint.supportMethods.includes(params.endpoint)) throw new Error('Unsupport Endpoint Type ' + params.endpoint);
            ['name', 'accept', 'response'].forEach((key) => {
                const value = RestfulEndpoint.validate(key, params[key]);
                if (!value) throw new Error(`Invalid ${key}: ${params[key]}`);
                this[key] = value;
            });
            this.method = RestfulEndpoint.methodMap[params.endpoint];
            this.keyed = RestfulEndpoint.keyed.includes(params.endpoint);
        }

        getFullPath() {
            const { name } = this;
            return `/api/restful/${name}`;
        }

    }

    return RestfulApiDefinitions;
};
