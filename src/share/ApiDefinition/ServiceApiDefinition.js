import { SimpleSchema } from "../utils/SchemaUtil";

const HttpMethod = {
    get: 'get',
    post: 'post',
    put: 'put',
    delete: 'delete'
};

export default function createServiceApiDefinitionBuilder({ ApiVersion, ApiRoute }) {

    class ServiceApiDefinitions {
        constructor(details) {
            Object.keys(details).forEach((key) => {
                try {
                    const item = new ServiceVersion(key, details[key]);
                    this[key] = item;
                } catch (err) {
                    console.error(`When creating Service API definition: ${err}`);
                }
            });
        }

        getApi(url) {
            const paths = url.split('/').slice(2);
            const version = this[paths[0]];
            if (!version) return;
            const router = version[paths[1]];
            if (!router) return;
            return router[paths[2]];
        }
    }
    class ServiceVersion {

        constructor(version, params) {
            Object.keys(params).forEach(route => {
                this[route] = new ServiceRoute(version, route, params[route]);
            });
        }

    }

    class ServiceRoute {
        constructor(version, route, params) {
            Object.keys(params).forEach(endpoint => {
                this[endpoint] = new ServiceEndpoint({ version, route, ...params[endpoint] });
            });
        }
    }

    class ServiceEndpoint {

        static validate(key, value) {
            switch (key) {
                case 'version': return ApiVersion[value];
                case 'route': return ApiRoute[value];
                case 'path': return value;
                case 'method': return HttpMethod[value];
                case 'accept': return SimpleSchema(value);
                case 'response': return SimpleSchema(value);
            }
        }

        constructor(params) {
            ['version', 'route', 'path', 'method', 'accept', 'response'].forEach((key) => {
                const value = ServiceEndpoint.validate(key, params[key]);
                if (!value) throw new Error(`Invalid ${key}: ${params[key]}`);
                this[key] = value;
            });
        }

        getFullPath() {
            const { version, route, path } = this;
            return `/api/${version}/${route}${path}`;
        }

    }

    return ServiceApiDefinitions;
};
