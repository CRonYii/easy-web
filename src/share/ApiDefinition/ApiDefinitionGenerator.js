import createServiceApiDefinitionBuilder from "./ServiceApiDefinition";
import createRestfulApiDefinitionBuilder from "./RestfulApiDefinition";

export const makeApiDefinition = (dataDefs, { serviceApiDefs, restfulApiDefs }) => {
    const { ApiVersion, ApiRoute, RestfulEntity } = dataDefs;

    const ServiceApiDefinitions = createServiceApiDefinitionBuilder({ ApiVersion, ApiRoute });
    const RestfulApiDefinitions = createRestfulApiDefinitionBuilder({ RestfulEntity });

    const ServiceApis = new ServiceApiDefinitions(serviceApiDefs);
    const RestfulApis = new RestfulApiDefinitions(restfulApiDefs);

    return [ServiceApis, RestfulApis];
};

export const getAllApisFromDefinition = function (obj, lastLevel, level = 1) {
    const result = {};

    if (!lastLevel) {
        if (obj.constructor.name === 'ServiceApiDefinitions') {
            lastLevel = 3;
        } else if (obj.constructor.name === 'RestfulApiDefinitions') {
            lastLevel = 2;
        }
    }

    if (level >= lastLevel) {
        return obj;
    }

    for (let i in obj) {
        if (!obj.hasOwnProperty(i)) continue;

        if ((typeof obj[i]) == 'object') {
            const flatObject = getAllApisFromDefinition(obj[i], lastLevel, level + 1);
            for (let j in flatObject) {
                if (!flatObject.hasOwnProperty(j)) continue;

                result[i + '.' + j] = flatObject[j];
            }
        } else {
            result[i] = obj[i];
        }
    }
    return result;
};