import { makeApiDefinition } from "./ApiDefinitionGenerator";
import { SimpleSchema } from "../utils/SchemaUtil";

const ApiVersion = {
    v1: 'v1'
};

const ApiRoute = {
    userServices: 'userServices'
};

const RestfulEntity = {
    users: 'users',
    logs: 'logs'
};


const serviceApiDefs = {
    [ApiVersion.v1]: {
        [ApiRoute.userServices]: {
            login: {
                method: 'post',
                path: '/login',
                accept: {
                    username: String,
                    password: String
                },
                response: {}
            },
            logout: {
                method: 'post',
                path: '/logout',
                accept: {},
                response: {}
            },
            test: {
                method: 'post',
                path: '/test',
                accept: {
                    message: String
                },
                response: {
                    message: String
                }
            }
        }
    }
};

const restfulApiDefs = {
    [RestfulEntity.users]: {
        index: {
            accept: {},
            response: [{
                username: String,
                email: String,
                role: [Map, { optional: true }],
                lastLogin: Date
            }]
        },
        create: {
            accept: {
                username: String,
                password: String,
                email: String
            },
            response: {}
        }
    },
    [RestfulEntity.logs]: {
        index: {
            accept: {
                level: [String, { optional: true }],
                timestamp: [String, { optional: true }],
            },
            response: [{
                timestamp: Date,
                level: String,
                message: String,
                role: [Map, { optional: true }]
            }]
        }
    }
};

const [ServiceApis, RestfulApis] = makeApiDefinition({ ApiVersion, ApiRoute, RestfulEntity }, { serviceApiDefs, restfulApiDefs });

export { ServiceApis, RestfulApis };