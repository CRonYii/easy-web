const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const clientSrcPath = 'src/client';
const clientBuildPath = 'build/client';

module.exports = (env = {}) => ({
    entry: [path.join(__dirname, clientSrcPath, 'index.js')],
    output: {
        path: path.join(__dirname, clientBuildPath),
        filename: '[name].[chunkhash].js',
    },
    mode: env.NODE_ENV || 'development',
    devtool: env.NODE_ENV === 'production' ? false : 'cheap-module-source-map',
    resolve: {
        modules: [path.resolve(__dirname, clientSrcPath), 'node_modules']
    },
    module: {
        rules: [
            {
                // this is so that we can compile any React,
                // ES6 and above into normal ES5 syntax
                test: /\.(js|jsx)$/,
                // we do not want anything from node_modules to be compiled
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(css)$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                ]
            },
            {
                test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
                loaders: ['file-loader']
            }
        ]
    },
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    plugins: [
        new ErrorOverlayPlugin(),
        new CleanWebpackPlugin(path.join(__dirname, clientBuildPath)),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src/client', 'index.html')
        }),
        new BundleAnalyzerPlugin({
            analyzerMode: env.analyzer || 'disabled',
            generateStatsFile: env.generateStatsFile || false,
            statsOptions: { source: false }
        }),
    ]
});